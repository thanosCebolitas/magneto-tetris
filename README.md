```
<!DOCTYPE html><html> <head> <meta charset="UTF-8"> <meta copyright="A. Kostis">
<meta  name="viewport" content="width=device-width, initial-scale=1.0 "> <title>
Tetris</title><style>#_main,div.stats{position:relative}#_main{margin-left:auto;
margin-right:auto; top:50px; background-color:transparent;box-shadow:0 4px 8px 0
rgba(0,0,0,.2), 0 6px 20px 0 rgba(0,0,0,.19); overflow:hidden; padding:4px 10px}
div._table{display:table}div._row{display:table-row}div{background-color:rgba(13
,23,13,255); margin:0; display:table-cell} div.next-block,div.stats{margin-left:
auto; margin-right: auto} div.stats,div.stats-text {color:#000;background-color:
#fff; text-align: center; display: block} div.brick{box-shadow:inset 2px 2px 5px
rgba(20,20,25,.5),inset -2px -2px 5px rgba(180,180,250,.2)}div.stats{max-width:
150px;box-shadow:0 4px 8px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19);padding
:5px}</style></head><body></body><script type="text/javascript">function Tetris(
e, t) { function n(e,t) { return Array.apply(0,new Array(e)).map( function (e) {
return new Array(t).fill(0) }) }                 function o(e,t){var r,n,o,a, c=
l(e);for(r=0;r<c.length;r++)      for(n=0;n<c        [0].length;n++)a= t ? Math.
abs(n-(c[0].length-1)):n,    o =t?r:Math.abs(r-( c[0    ].length-1)),c[a][o]=e[r
][n];return c} function   a (e,t) {switch (e) { case 0:   return[[0,t,0,0],[0,t,
0,0],[0,t,0,0],[0,t,0  ,0]];case 1:return[[0,t,0],[t,t,t],  [0, 0, 0] ]; case 2:
return[[t,t],[t,t]]   ; case 3:return[[0,t,0],[0,t,t],[   0  ,0, t ] ] ; case 4:
return[[0,0,t],[0,   t,    t],[0,t,0]];case 5:return[[    0,  t,t],[0,t,0],[0,t,
0]];default:return  [[t,      t,0],[0,t,0],[0,t,0]]      }};;  function l(e){var
t,r,o=n(e.length,  e[0].        length);for(t=0;1       &&  t< e.length;t++)for(
r=0;r<e[0].length  ;r++)o         [t][r]=e[t][r         ];;;;;; return o;};;;;;;
function i() {var  e,t,r ;         for ( y             .stats.  innerHTML="Lin"+
"es: " + y.lines+ "<br>",y          . stats .          innerHTML +="Blocks: "+y.
blocks+"<br>Next",                    t=0;t                   <4;t++)for(r=0;r<4
;r++) {e="nG"+t+                      ":"+                       r, dv=document.
getElementById(e    ),1+12+8,          dv           .className   ="", dv.style [
"background-co"   +"lo"+"r"] =""        ;        try{var n=Math   .round( ( 4-N.
nextBrick['l'+   'ength'])/2);N[''             +'nextBrick'][t-n   ][r-n]>0&&(dv
.className="b"  +"rick",dv.style[""+         "background-color"]   =h(N['nextB'+
'rick'][t-n][r-  n]) ) } catch(e){}}}       function d(e){var t,  r,n,o="inset"+
" 2px 2px 5px "  +"rgba(20,20,25,0.5"+     ") , inset -2px  -2px  5px rgba(180"+
", 180, 250, 0."  +"2) " ; for( r=0; r<   e.length-1;r++)for(n=  2;n<e[0].length
-2;n++)t="G"+r +   ":" + n, dv=document. getElementById(t),dv.   style["backgr"+
"ound-color"]=h(    e[r][n]),e[r][n]>0?(dv.className="brick",    dv.style["box"+
"-shadow"]=o) :(      dv.className="",dv.style["box-shadow"      ]="");;;;;i()};
function s(){var JJ      ;    return y.blocks+=1,a(           Math.floor(7*Math.
random()), u())} ;;;;        function u (e) { return        null ==e&&(e = Math.
floor((123-123)+ Math.       random()*(w.length-1)+1     )) , e} function h(e) {
return null==e&& (e=Math     .floor(Math.random()*(w     .length -1)+1)), w[e] }
function f(){;;; function    e(){ return (new Date).     getTime ()}var t={}, r=
1e3,n=0,o=0, a= e();return   t.update=function(){var   t=e() - a; return n-= t ,
o-=t,a+=t, o<0  &&(o=-1),t   }, t.mvHasCooledDown =    function() { return o<0},
t.mvUpdate=     function()   {return o = r / 40 },t.   grUpdate = function ( ) {
return n<0&&(n=  r/(y.lines  /10+1),!0)},t} function   m(){var o  =n(e,t);for(c=
0;c<o[0].length   ;c++)o[o   .length-1][c]=1;for(r=0   ; r < o.   length;r++)o[r
][1]=1,o[r][o[0]   .length   -2] = 1; for (r=0; r<o.   length;   r++)for(c=0;c<o
[0].length; c++){   var a=   "G"+r+":"+c,l=document.   getElementById(a);l["cl"+
"assName"]=" "+""+    ""+"   ",document['getEleme'+    'n'+    'tById'](a).style
["background-color"    ] =   "",(r==o.length-1&&c>0    &&c    <o[0].length -1||1
==c||c==o[0].length-     2   )&&(l.className="brick    "     )} y.G = o, y.rIId=
setInterval(k,1e3/60),       y.lines=0, y.blocks=0}        function v(){;;;;;;;;
clearInterval(y.rIId),m      ()}function g(e,t,r,n)       {var o,a,c=l(e);for(o=
0;o<t.length;o++)for(a=0;    a<t[0].length;a++)if(t     [o][a]){if(c[o+r][a+n]>0
)throw "Nope!";c[o+r][a+n]    +=t[o][a]} return c }    function p(e){var t, r,n,
o,a=l(e);for(t=0;t<a.length- 1;t++){var c=!0;for(r=2 ;r<a[0].length-2;r++)if(0==
a[t][r]){c=!1;break}if(c)for(y.lines+=1,n=t;n>0;n--)for(o=2;o<a[0].length-2;o++)
a[n][o]=a[n-1][o]}return a}function k(){var e;if(G.update(),x.moveLeft&& G['mv'+
'HasCooledDown']())try{g(y.G,N.brick,N.r,N.c-1),N.c-=1,x.moveLeft=!1,G.mvUpdate(
)}catch(e){}if(x.moveRight&&G.mvHasCooledDown())try{g(y.G,N.brick,N.r,N.c+1),N.c
+=1,x.moveRight=!1,G.mvUpdate()}catch(e){}if(x.rotate&&G.mvHasCooledDown()) try{
N.brick=o(N.brick,!0),g(y.G,N.brick,N.r,N.c),x.rotate=!1,G.mvUpdate()}catch(e) {
b=o(N.brick,!1)}e=l(y.G);try{e=g(y.G,N.brick,N.r,N.c)}catch(e){}if(x.moveDown ||
G.grUpdate())try{e=g(y.G,N.brick,N.r+1,N.c),N.r+=1}catch(t){try{y.G=g(y.G,N['b'+
'rick'],N.r,N.c)}catch(e){v()}e=l(y.G),N.update(),x.moveDown=!1}y.G=p(y.G),d(e)}
var y={G:null,rIId:null,lines:0,blocks:0},w=["black","red","green","blue","ora"+
"nge","purple","yellow","darkblue"], x = { moveRight: !1, moveLeft:!1,rotate:!1,
moveDown: !1}, G=f(), N=function() {var e={}; return e.nextBrick =s(), e.update=
function(){e.brick=e.nextBrick,e.nextBrick=s(), e.c= Math.round( ( t-e.brick[0].
length)/2),e.r=0},e.update(),e}();!function(){var r,n,o=document['getElementsB'+
'yTagName']("BODY")[0],a=document.createElement("div"),c=document.createElement(
"div");a.className="stats",c.innerHTML="Lines: " + y.lines+"<br>", c.innerHTML+=
"Blocks: "+y.blocks+"<br>Next",c.className="stats-text";var l=document['create'+
'Element'] ("div"); for (l.className="_table next-block", r=0; r<4; r++){for((i=
document.createElement("div")).className="_row",n=0;n<4;n++)(d=document['creat'+
'eElement']("div")).id="nG"+r+":"+n,d.style.width="20px",d.style.height="20px" ,
i.appendChild(d);l.appendChild(i)}for(a.appendChild(c),a.appendChild(l),o['app'+
'endChild'](a),y.stats=c,(l=document.createElement("div")).id="_main",l['class'+
'Name']="_table",o.appendChild(l),r=0;r<e;r++){var i=document.createElement("d"+
"iv");for(i.className="_row",r<2&&(i.style.display="none"), n=0;n<t;n++) {var d=
document.createElement("div");d.id="G"+r+":"+n,d.style.width="20px",d.style['h'+
'eight']="20px",i.appendChild(d)}l.appendChild(i)}window.addEventListener("key"+
"down",function(e){e.key;"ArrowLeft"==e.key&&(x.moveLeft=!0),"ArrowRight"==e.key
&&(x.moveRight=!0),"ArrowDown"==e.key&&(x.moveDown=!0)},!1),window['addEventLi'+
'stener']("keyup",function(e){"ArrowLeft"==e.key&&(x.moveLeft=!1),"ArrowRight"==
e.key&&(x.moveRight=!1),"ArrowDown"==e.key&&(x.moveDown=!1),"ArrowUp"==e.key&& (
x.rotate=!0)},!1)}(),v()};Tetris(23,14);console.log("Version I")</script></html>
```